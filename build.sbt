//=================================================================================
// PROJECTS
//=================================================================================
lazy val file_analyzer = 
    (project in file("."))
    .settings(fileAnalyzerSettings)

//=================================================================================
// SETTINGS
//=================================================================================
lazy val fileAnalyzerSettings = Seq(
    name := "Scala Text File Analyzer",
    version := (version in ThisBuild).value,
    scalaVersion := "2.13.1",
    mainClass in (Compile, run) := Some("org.jakubf.fileanalyzer.Main"),
    libraryDependencies ++= fileAnalyzerDependencies
)

//=================================================================================
// DEPENDENCIES
//=================================================================================
lazy val fileAnalyzerDependencies = Seq(
    "org.scalactic" %% "scalactic" % "3.1.0",
    "org.scalatest" %% "scalatest" % "3.1.0" % "test"
)