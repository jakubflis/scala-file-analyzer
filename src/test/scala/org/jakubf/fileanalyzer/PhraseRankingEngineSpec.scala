package org.jakubf.fileanalyzer

import org.jakubf.fileanalyzer.analyzers.PhraseRankingEngine
import org.jakubf.fileanalyzer.models.{DetailedFile, FileIndex}
import org.jakubf.fileanalyzer.traits.OutputSink
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class PhraseRankingEngineSpec extends AnyFlatSpec with Matchers with GivenWhenThen {
  private trait MockOutput extends OutputSink {
    var outputMessages: Seq[String] = Seq()
    override def println(message: String): Unit = {
      outputMessages = outputMessages :+ message
    }
  }

  "Phrase Ranking Engine" should "generate correct ranking results" in {
    Given("A File Index with different DetailedFiles")
    val detailedFiles = List(DetailedFile("Test1.txt", Map("dog" → 1, "parrot" → 1)),
                            DetailedFile("Test2.txt", Map("mouse" → 1)),
                            DetailedFile("Test3.txt", Map("cat" → 1, "mouse" → 1)))
    val mockFileIndex = FileIndex(detailedFiles)

    And("A simple input phrase")
    val inputPhrase = "Cat Mouse"

    When("PhraseRankingEngine analyzes given FileIndex")
    val phraseRankingEngine = new PhraseRankingEngine(mockFileIndex) with MockOutput
    phraseRankingEngine.computeRanking(inputPhrase)

    Then("Output messages should give correct rankings")
    val expectedResult = "Test3.txt - 100.0%, Test2.txt - 50.0%, Test1.txt - 0.0%"
    val result = phraseRankingEngine.outputMessages.mkString(" ")

    result should equal (expectedResult)
  }

  "Phrase Ranking Engine" should "omit duplicate input words" in {
    Given("A File Index with one DetailedFile")
    val mockFileIndex = FileIndex(List(DetailedFile("Test1.txt", Map("cats" → 1, "dogs" → 1))))

    And("An input phrase that contains duplicated words")
    val inputPhrase = "Cats cats parrots"

    When("Phrase Ranking Engine prepares a word ranking")
    val phraseRankingEngine = new PhraseRankingEngine(mockFileIndex) with MockOutput
    phraseRankingEngine.computeRanking(inputPhrase)

    Then("Phrase Ranking Engine should ignore duplicated words")
    val expectedResult = "Test1.txt - 50.0%"
    val result = phraseRankingEngine.outputMessages.mkString(" ")

    result should equal (expectedResult)
  }
}
