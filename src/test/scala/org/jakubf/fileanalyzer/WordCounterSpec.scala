package org.jakubf.fileanalyzer

import org.jakubf.fileanalyzer.analyzers.WordCounter
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class WordCounterSpec extends AnyFlatSpec with Matchers with GivenWhenThen {
  "WordCounter" should "count words correctly in proper input sequence" in {
    Given("Test string sequence containing phrases")
    val inputList = List("Cat", "dog", "worm", "Cat")

    When("Counting words in input sequence")
    val countedWords = WordCounter.countWordsInFile(inputList.iterator)

    Then("Result should be correct")
    val expectedResult = Map("Cat" → 2, "dog" → 1, "worm" → 1)

    expectedResult should be (countedWords)
  }

  "WordCounter" should "count words correctly in empty input sequence" in {
    Given("An empty input sequence")
    val inputList = List()

    When("Counting words in that input sequence")
    val countedWords = WordCounter.countWordsInFile(inputList.iterator)

    Then("Result should be empty")
    val expectedResult = Map()

    expectedResult should be (countedWords)
  }

  "WordCounter" should "count case sensitive words correctly" in {
    Given("An input sequence with same words but with different case sensitivity")
    val inputList = List("Cat", "cat", "Worm", "worm")

    When("Counting words in that input sequence")
    val countedWords = WordCounter.countWordsInFile(inputList.iterator)

    Then("Result should be grouped by case sensitivity")
    val expectedResult = Map("Cat" → 1, "cat" → 1, "Worm" → 1, "worm" → 1)

    expectedResult should be (countedWords)
  }
}
