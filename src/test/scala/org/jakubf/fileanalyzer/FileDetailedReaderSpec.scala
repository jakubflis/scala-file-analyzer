package org.jakubf.fileanalyzer

import java.io.File

import org.jakubf.fileanalyzer.readers.FileDetailedReader
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class FileDetailedReaderSpec extends AnyFlatSpec with Matchers with GivenWhenThen {
  "FileDetailedReader" should "prepare a proper DetailedFile" in {
    Given("Test file resource and a FileDetailedReader")
    val testFile =  new File(getClass.getResource("/File1.txt").getFile)
    val fileDetailedReader = new FileDetailedReader()

    When("Reading that file resource by the reader")
    And("Preparing that file for word count")
    val detailedFile = fileDetailedReader.readFileInDetail(testFile)

    Then("Produced detailed file should match expected result")
    val expectedWordCount = Map("ala" → 2, "ma" → 1, "kota" → 1)
    detailedFile.fileName should be (testFile.getName)
    detailedFile.wordCount should be (expectedWordCount)
  }

  "FileDetailedReader" should "prepare a proper FileIndex" in {
    Given("Directory with test file resources and a FileDetailedReader")
    val testResourceDirectory = new File(getClass.getResource("/File1.txt").getFile)
      .getAbsolutePath.replace("/File1.txt", "")

    val fileDetailedReader = new FileDetailedReader()

    When("Reading that directory by the reader")
    val fileIndex = fileDetailedReader.readFileIndex(testResourceDirectory)

    Then("Produced FileIndex should match expected result")
    fileIndex.files.size should be (3)
  }
}
