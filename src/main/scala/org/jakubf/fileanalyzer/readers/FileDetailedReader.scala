package org.jakubf.fileanalyzer.readers

import java.io.File

import org.jakubf.fileanalyzer.analyzers.WordCounter
import org.jakubf.fileanalyzer.exceptions.{DirectoryReadException, FileReadException}
import org.jakubf.fileanalyzer.models.{DetailedFile, FileIndex}

import scala.io.{BufferedSource, Source}
import scala.util.{Failure, Success, Try}

/** Reads whole directory of files or a single file.
 * Produces DetailedFile or FileIndex objects.
 */
class FileDetailedReader {
  type WordCount = Map[String, Int]

  /** Reads given directory path and produces FileIndex object
   *
   *  Takes only .txt files into consideration.
   *  @param directory - path to file directory
   *  @throws DirectoryReadException
   */
  def readFileIndex(directory: String): FileIndex = {
    Try(new File(directory)) match {
      case Success(directory) ⇒
        FileIndex(directory
          .listFiles
          .filter(filterOnlyTextFiles)
          .map(readFileInDetail)
          .toList)
      case Failure(e) ⇒
        throw DirectoryReadException(e.getMessage)
    }
  }

  /** Converts given File object into DetailedFile object
   *
   *  @param file - java.io.File object
   *  @throws FileReadException
   */
  def readFileInDetail(file: File): DetailedFile = {
    Try(Source.fromFile(file, "UTF-8")) match {
      case Success(bufferedFile) ⇒
        DetailedFile(file.getName, WordCounter.countWordsInFile(prepareLinesForCounting(bufferedFile)))
      case Failure(e) ⇒
        throw FileReadException(e.getMessage)
    }
  }

  private def filterOnlyTextFiles(file: File): Boolean = {
    file.getName.endsWith(".txt")
  }

  private def prepareLinesForCounting(bufferedFile: BufferedSource): Iterator[String] = {
    bufferedFile
      .getLines()
      .map(_.toLowerCase)
      .map(removePunctuationCharacters)
  }

  // TODO - Extract removePunctuationCharacters and PhraseRankingEngineSpec.prepareInputPhraseForAnalysis functions
  private def removePunctuationCharacters(input: String): String = {
    input
      .replaceAll("\\.", "")
      .replaceAll("""[\p{Punct}&&[^.]]""", "")
  }
}
