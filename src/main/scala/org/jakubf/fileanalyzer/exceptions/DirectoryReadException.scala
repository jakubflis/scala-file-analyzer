package org.jakubf.fileanalyzer.exceptions

/** Threw when directory read operation fails.
 *
 *  @param errorMessage - message that will be printed to the user.
 */
case class DirectoryReadException(errorMessage: String) extends Error
