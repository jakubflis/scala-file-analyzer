package org.jakubf.fileanalyzer.exceptions

/** Threw when file read operation fails.
 *
 *  @param errorMessage - message that will be printed to the user.
 */
case class FileReadException(errorMessage: String) extends Error
