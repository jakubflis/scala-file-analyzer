package org.jakubf.fileanalyzer.exceptions

/** Threw when directory path doesn't point to any directory
 *
 */
class MissingDirectoryPathException extends Error {
  override def getMessage: String = "Directory path not provided!"
}
