package org.jakubf.fileanalyzer.traits

/** Trait for defining output sink.
 *
 *  For example, an input stream can be a Console println operation (default)
 */
trait OutputSink {
  def println(message: String): Unit = Console.println(message)
  def print(message: String): Unit = Console.print(message)
}
