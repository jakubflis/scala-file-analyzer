package org.jakubf.fileanalyzer.traits

import scala.io.StdIn.readLine

/** Trait for defining input stream.
 *
 *  For example, an input stream can be a Console readLine operation (default)
 */
trait InputSource {
  def readInputLine(): String = readLine
}
