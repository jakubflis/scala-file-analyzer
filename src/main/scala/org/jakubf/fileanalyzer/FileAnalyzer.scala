package org.jakubf.fileanalyzer

import org.jakubf.fileanalyzer.analyzers.PhraseRankingEngine
import org.jakubf.fileanalyzer.models.FileIndex
import org.jakubf.fileanalyzer.traits.{InputSource, OutputSink}

/** Main Analyzer program. Analyzes program for given FileIndex objects
 *
 *  @param fileIndexSource - factory for creating FileIndex object that will be analyzed
 */
class FileAnalyzer(fileIndexSource: () ⇒ FileIndex) extends OutputSink with InputSource {
  lazy val fileIndex: FileIndex = fileIndexSource()
  lazy val phraseRankingFileAnalyzer = new PhraseRankingEngine(fileIndex)

  /** Runs main program loop.
   *
   *  Awaits for user input from source defined by InputSource.
   *  Probably one of the rarest places to use 'return' keyword in Scala.
   */
  def awaitInput(): Unit = {
    print("search> ")
    readInputLine() match {
      case ":quit" ⇒ return
      case phrase if phrase.length > 0 ⇒
        phraseRankingFileAnalyzer.computeRanking(phrase)
        awaitInput()
      case phrase if phrase.isEmpty ⇒
        awaitInput()
    }
  }
}
