package org.jakubf.fileanalyzer.models

/** Wrapper for DetailedFile objects. Represents a directory of files
 *
 *  @param files List of DetailedFiles
 */
case class FileIndex(files: List[DetailedFile])
