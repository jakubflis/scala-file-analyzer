package org.jakubf.fileanalyzer.models

/** In-memory representation of a text file contents
 *
 *  @param fileName name of the file
 *  @param wordCount Key-value map that contains word and number of that word's occurrences
 *                   WordCount map should not contain any whitespaces or punctuation characters.
 *                   Also, all WordCount entries should be lowercase.
 */
case class DetailedFile(fileName: String, wordCount: Map[String, Int])
