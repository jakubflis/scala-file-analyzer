package org.jakubf.fileanalyzer

import org.jakubf.fileanalyzer.exceptions.MissingDirectoryPathException
import org.jakubf.fileanalyzer.readers.FileDetailedReader

object Main extends App {
    args.headOption match {
        case Some(directoryPath) ⇒
            val fileIndexSource = () ⇒ new FileDetailedReader().readFileIndex(directoryPath)
            new FileAnalyzer(fileIndexSource).awaitInput()
        case _ ⇒
            throw new MissingDirectoryPathException
    }
}
