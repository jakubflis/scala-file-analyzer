package org.jakubf.fileanalyzer.analyzers

import org.jakubf.fileanalyzer.models.{DetailedFile, FileIndex}
import org.jakubf.fileanalyzer.traits.OutputSink

/** Computes phrase ranking for given FileIndex.
 *
 *  @param fileIndex - contains DetailedFiles that will be analysed
 */
class PhraseRankingEngine(fileIndex: FileIndex) extends OutputSink {
  /** Computes ranking among fileIndex for given phrase.
   *
   *  Phrase is firstly cleaned and prepared for analysis.
   */
  def computeRanking(phrase: String): Unit = {
    val phraseWords = prepareInputPhraseForAnalysis(phrase)
    def computeResultMap(resultMap: Map[String, Double], file: DetailedFile): Map[String, Double] = {
      val result = numberOfOccurrencesInFile(file, phraseWords) match {
        case matchedWords if matchedWords > 0 ⇒ computePercentageResult(matchedWords, phraseWords.length)
        case _ ⇒ 0.0
      }
      resultMap + (file.fileName → result)
    }

    val result = fileIndex.files.foldLeft(Map.empty[String, Double])(computeResultMap)
      .toSeq
      .sortWith(sortByResultDescending)
      .map(constructResultDescription)
      .mkString(", ")

    println(result)
  }

  // TODO - Extract prepareInputPhraseForAnalysis and FileDetailReader.removePunctuationCharacters functions
  private def prepareInputPhraseForAnalysis(phrase: String): Array[String] = {
    phrase.split(" ")
      .map(_.toLowerCase)
      .map(_.replaceAll("""[\p{Punct}&&[^.]]""", ""))
      .distinct
  }

  private def sortByResultDescending(leftValue: (String, Double), rightValue: (String, Double)): Boolean = {
    leftValue._2 > rightValue._2
  }

  private def constructResultDescription(value: (String, Double)): String = {
    value match {
      case (file, result) ⇒ s"$file - $result%"
    }
  }

  private def numberOfOccurrencesInFile(file: DetailedFile, phraseWords: Array[String]): Int = {
    phraseWords.foldLeft(0) { (matchCounter, word) ⇒
      matchCounter + (if (file.wordCount.contains(word)) 1 else 0)
    }
  }

  private def computePercentageResult(matchedWords: Int, totalWords: Int): Double = {
    (matchedWords.toDouble / totalWords.toDouble) * 100
  }
}
