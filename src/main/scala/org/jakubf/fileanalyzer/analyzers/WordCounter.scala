package org.jakubf.fileanalyzer.analyzers

/** Auxiliary object for performing word count operation. */
object WordCounter {
  type WordCount = Map[String, Int]
  /** Creates a map that's a representation of words distribution
   *
   *  @param wordLines iterable object of subsequent lines of words that will be analysed and counted
   */
  def countWordsInFile(wordLines: Iterator[String]): WordCount = {
    wordLines
      .flatMap(_.split(" "))
      .foldLeft(Map.empty[String, Int])(addOrUpdateMap)
  }

  private def addOrUpdateMap(wordCount: WordCount, word: String): WordCount = {
    wordCount + (word → (wordCount.getOrElse(word, 0) + 1))
  }
}
