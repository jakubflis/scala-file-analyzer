# Text File Analyzer

An application for analyzing text file contents.

## Structure

The application is based on a set of Analyzers and Readers. In current shape, there's only one of each, but with room for more future implementations.

## Running the application

To run the application:

1. Execute `sbt "run {DIRECTORY_PATH}"` in Terminal window,
2. Optionally, if you don't want to use SBT's batch mode, open the SBT console and execute `run {DIRECTORY_PATH}` command (notice no quote signs),
3. After a while you should see a `search> `  message on the terminal window.

You can now start writing your phrases that will be analyzed against files in passed {DIRECTORY_PATH}.

## Application characteristics

* Input phrase is pre-processed before the analyzis. All words are stripped from punctiation characters and white spaces. I.e., `Cats. And Dogs!` will be analyzed as a list of words `List("cast", "and", "dogs")`,
* A word is defined as a sequence of chars between two spaces (or one space, if that's the first or the last word in input sequence).

## Prerequisites

* Scala
* SBT
* A set of .txt files to analyze

## TODOs

In order of priority:

* Write integration tests between Readers and Analyzers (especially for `FileAnalyzer` class),
* Write unit tests for cases when exceptions are being thrown,
* Remove some code smells (i.e. puncutary removal code),
* Implement better logging solution,
* Write documentation for running the application from JAR file,
* Consider CI/CD solutions (especially versioning).